import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources", // chemin vers les fichiers .feature
        plugin = { "pretty", "json:target/cucumber.json", "html:logs-execution/cucumber-reports.html" }
)
public class TestRunner {
}

package action;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleSearchPage {
    private final WebDriver driver;
    private final By searchBox = By.name("q");
    private final By acceptCookiesButton = By.id("W0wltc");

    public GoogleSearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get("https://www.google.com");
    }

    public void acceptCookies() {
        driver.findElement(acceptCookiesButton).click();
    }

    public void enterSearchTerm(String searchTerm) {
        driver.findElement(searchBox).sendKeys(searchTerm);
    }

    public void submitSearch() {
        driver.findElement(searchBox).sendKeys(Keys.RETURN);
    }

    public String getTitle() {
        return driver.getTitle();
    }
}

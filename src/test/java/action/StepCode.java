package action;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class StepCode {
    private WebDriver driver;
    private GoogleSearchPage googleSearchPage;

    @Before
    public void setup() throws MalformedURLException {
        String browser = System.getProperty("browser", "firefox");

        switch (browser.toLowerCase()) {
            case "chrome" -> {
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setHeadless(true);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/"), chromeOptions);
            }
            case "firefox" -> {
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setHeadless(true);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/"), firefoxOptions);
            }
            default -> throw new IllegalArgumentException("Browser non supporté : " + browser);
        }

        googleSearchPage = new GoogleSearchPage(driver);
    }

    @Given("I am on the Google search page")
    public void i_am_on_the_Google_search_page() {
        googleSearchPage.open();
    }

    @When("I accept the cookies")
    public void i_accept_the_cookies() {
        googleSearchPage.acceptCookies();
    }

    @And("I search for {string}")
    public void i_search_for(String string) {
        googleSearchPage.enterSearchTerm(string);
        googleSearchPage.submitSearch();
    }

    @Then("the page title should contain {string}")
    public void the_page_title_should_contain(String string) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleContains(string));
        String title = googleSearchPage.getTitle();
        Assert.assertTrue(title.contains(string));
    }

    @After
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                TakesScreenshot ts = (TakesScreenshot) driver;
                byte[] screenshot = ts.getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "screenshot");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (driver != null) {
            driver.quit();
        }
    }
}

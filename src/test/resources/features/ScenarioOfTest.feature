Feature: Google Search
  Scenario Outline: Searching for a term
    Given I am on the Google search page
    When I accept the cookies
    And I search for "<searchTerm>"
    Then the page title should contain "<searchTerm>"

    Examples:
      | searchTerm |
      | kenobi     |
      | anakin     |
      | padme      |
      | yoda       |
      | dooku      |
      | qui-gon    |
      | windu      |


# exemple pour scénario classque
#  Scenario: Searching for a term
#    Given I am on the Google search page
#    When I accept the cookies
#    And I search for "Kenobi"
#    Then the page title should contain "Kenobi"
